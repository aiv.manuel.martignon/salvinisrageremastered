import Phaser from "phaser"

class PlayerBullet extends Phaser.Physics.Arcade.Sprite {
    constructor(scene) {

        let spriteName
        let damage
        let speed
        let owner = scene.Player

        switch (owner.BulletToShoot) {
            case 0:
                spriteName = owner.BulletTypes[0]
                damage = 20
                speed = 200
                break
            case 1:
                spriteName = owner.BulletTypes[1]
                damage = 10
                speed = 160
                break
        }

        super(scene, owner.body.position.x, owner.body.position.y, spriteName)
        this.scene = scene
        this.Owner = owner
        this.Damage = damage
        this.Speed = speed

        this.scene.add.existing(this)
        this.scene.physics.world.enable(this)

        this.setScale(0.5)
        this.DestroyTimer = 7000
    }
}

export default PlayerBullet