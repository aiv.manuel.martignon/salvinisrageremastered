import Phaser from "phaser"

//i have decided to make this a boss bullet because the normal enemy is performing
//specific checks on death and is moving differently...
class BossBullet extends Phaser.Physics.Arcade.Sprite {
    constructor(scene) {
        //pick a random sprite for this enemy
        let randomNumber = Phaser.Math.Between(0, 1)
        let spriteName = "Immigrant" + randomNumber
        super(scene, -200, -200, spriteName) //base ctor

        this.scene = scene
        this.scene.add.existing(this)
        this.scene.physics.world.enable(this)
        this.setScale(0.8)

        this.MaxLife = 100
        this.Life = this.MaxLife
        this.DestroyTimer = 10000

        this.LifeBar = this.scene.LifeBars.create()
        this.scene.add.existing(this.LifeBar)
        this.scene.physics.world.enable(this.LifeBar)

        this.LifeBar.setScale(1.5, 0.15)
        let lifeX = this.body.position.x + this.body.halfWidth
        let lifeY = this.body.position.y + this.body.height + 5
        this.LifeBar.setPosition(lifeX, lifeY)

        this.LifeBar.Owner = this

        let enemy = this
        //destroy this "bullet" and its lifebar after DestroyTimer millisecs
        this.scene.time.addEvent({
            delay: this.DestroyTimer,
            callback: function () {
                enemy.LifeBar.destroy()
                enemy.destroy()
            }
        })
    }

    GetDamage(damage) {
        this.Life -= damage
        this.LifeBar.scaleX = (this.Life * 1.5) / this.MaxLife
        if (this.Life <= 0)
        {
            this.scene.PlayerScore++
            this.scene.ScoreText.setText("Sbarchi Respinti: " + this.scene.PlayerScore)
            this.LifeBar.destroy()
            this.destroy()
        }
    }
}
export default BossBullet