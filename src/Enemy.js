import Phaser from "phaser"

//the normal enemy spawns from the top side of the screen and moves down, with a random x modifier
class Enemy extends Phaser.Physics.Arcade.Sprite {
    constructor(scene) {
        let StartingPositionX = Phaser.Math.Between(10, scene.game.config.width - 10)
        let StartingPositionY = -15
        //pick a random sprite
        let randomNumber = Phaser.Math.Between(0, 1)
        let spriteName = "Immigrant" + randomNumber
        super(scene, StartingPositionX, StartingPositionY, spriteName) //base ctor

        this.scene = scene
        this.scene.add.existing(this)
        this.scene.physics.world.enable(this)
        this.setScale(0.8)

        this.Speed = 65
        this.MaxAdditionalRandomSpeedY = 50
        this.MaxLife = 100
        this.Life = this.MaxLife
        this.DestroyTimer = 10000

        //use phaser's factory to create the lifebar for this enemy, and set its properties
        this.LifeBar = this.scene.LifeBars.create()

        this.LifeBar.setScale(1.5, 0.15)
        let lifeX = this.body.position.x + this.body.halfWidth
        let lifeY = this.body.position.y + this.body.height + 5
        this.LifeBar.setPosition(lifeX, lifeY)

        this.LifeBar.Owner = this

        this.scene.add.existing(this.LifeBar)
        this.scene.physics.world.enable(this.LifeBar)

        let enemy = this
        this.scene.time.addEvent({
            delay: this.DestroyTimer,
            callback: function () {
                enemy.LifeBar.destroy()
                enemy.destroy()
            }
        })
    }

    Attack() {
        this.body.setVelocityY(this.Speed + Phaser.Math.Between(0, this.MaxAdditionalRandomSpeedY))
        this.body.setVelocityX(Phaser.Math.Between(-25, 25))
    }

    GetDamage(damage) {
        this.Life -= damage
        this.LifeBar.scaleX = (this.Life * 1.5) / this.MaxLife
        if (this.Life <= 0) {
            this.scene.PlayerScore++
            this.scene.ScoreText.setText("Sbarchi Respinti: " + this.scene.PlayerScore)
            
            if(this.scene.PlayerScore % 5 == 0)
            {
                this.scene.SpawnEnemyEvent.delay -= 50
            }

            if(this.scene.PlayerScore == 10)
            {
                this.scene.SpawnEnemyEvent.destroy()
                this.scene.SpawnBoss()
            }

            this.LifeBar.destroy()
            this.destroy()
        }
    }
}
export default Enemy