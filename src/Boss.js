import Phaser from "phaser"

class Boss extends Phaser.Physics.Arcade.Sprite {

    constructor(scene) {
        let BossStartingX = scene.game.config.width / 2
        let BossStartingY = -100
        super(scene, BossStartingX, BossStartingY, "Boss") //call base ctor
        
        this.scene = scene
        this.scene.add.existing(this)
        this.scene.physics.world.enable(this)
        this.setScale(1.8)
        this.Speed = 100
        this.ShootChangeCD = 2500
        this.LastShootChange = Date.now()
        this.ShootGroups = ["Down-Straight", "Radial"]
        this.AvailableShootGroups = []
        this.AvailableShootGroups.push(this.ShootGroups[0], this.ShootGroups[1])
        this.CurrentShootGroup = 0

        this.ShootGroupsProperties = {
            "Down-Straight": { "BulletSpeed": 200, "NBullets": 10, "Interval": 350 },
            "Radial": { "BulletSpeed": 200, "NBullets": 15 },
            "Down-Alternate": { "BulletSpeed": 300, "NBullets": 20, "Interval": 350 },
            "Radial-Alternate": { "BulletSpeed": 300, "NBullets": 15, "Interval": 200 },
        }
        this.LastInterval = Date.now()
        this.BulletsShot = 0

        this.IsShooting = false

        //let's make him less dumb...
        this.FSM = ["Entering", "Fighting"]
        this.CurrentFSMState = this.FSM[0]

        this.Phases = ["Phase1", "Phase2", "Phase3"]
        this.CurrentPhase = this.Phases[0]

        this.Directions = ["Left", "Right"]
        this.CurrentDirection = this.Directions[0]

        this.MaxLife = 1500
        this.Life = this.MaxLife

        this.LifeBar = this.scene.LifeBars.create()

        let lifeY = scene.game.config.height - 15
        this.LifeBar.setPosition(0, lifeY)
        this.LifeBar.setScale(50, 1.2)
        this.LifeBar.Owner = this
        
        //font to handle the boss fight progress
        this.scene.BossText = this.scene.add.text(0, lifeY - 50, 'ONG MORTE NERA: ' + this.Life + "/" + this.MaxLife, { fontFamily: "'Luckiest Guy'", fontSize: '20px', fill: '#000' });
    }

    GetDamage(damage) {
        this.Life -= damage
        this.scene.BossText.setText("ONG MORTE NERA: " + this.Life + "/" + this.MaxLife)
        this.LifeBar.scaleX = (this.Life * 50) / this.MaxLife


        if (this.Life <= 1000 && this.CurrentPhase == this.Phases[0]) {
            this.CurrentPhase = this.Phases[1]
            this.AvailableShootGroups.push(this.ShootGroups[1])
            this.Speed = 200
            return
        }
        // else if (this.Life <= 250 && this.CurrentPhase == this.Phases[1]) {
        //     this.CurrentPhase = this.Phases[2]
        //     this.AvailableShootGroups.push(this.ShootGroups[3])
        //     this.Speed = 300
        //     return
        // }

        if (this.Life <= 0) {
            //destroy the boss and the life bar, TODO make the player win
            this.LifeBar.destroy()
            this.destroy()
        }

    }

    UpdateBehaviour() {
        switch (this.CurrentFSMState) {
            case "Entering":
                this.Enter()
                break;
            case "Fighting":
                this.Fight()
                break;
        }
    }

    Fight() {
        switch (this.CurrentDirection) {
            case "Left":
                this.MoveLeft()
                break
            case "Right":
                this.MoveRight()
                break
        }

        if (!this.IsShooting) {
            if (Date.now() - this.LastShootChange >= this.ShootChangeCD) {
                this.CurrentShootGroup = Phaser.Math.Between(0, this.AvailableShootGroups.length - 1)
                this.LastShootChange = Date.now()
                this.IsShooting = true
                this.Shoot()
            }
        }
        else { //the big boy is still performing his attack, so check the phase exc...
            this.Shoot()
        }

    }

    Shoot() {
        switch (this.CurrentShootGroup) {
            case 0:
                this.ShootDownStraight()
                break;
            case 1:
                this.ShootRadial()
                break;
            // case 2:
            //     this.ShootDownAlternate()
            //     break;
            // case 3:
            //     this.ShootDownAlternate()
            //     break;
        }
    }

    ShootDownStraight() {
        if (Date.now() - this.LastInterval >= this.ShootGroupsProperties["Down-Straight"].Interval) {
            let enemy = this.scene.BossBullets.create()

            let enemyX = this.body.position.x + this.body.halfWidth
            let enemyY = this.body.position.y + this.body.height

            enemy.setPosition(enemyX, enemyY)
            enemy.body.velocity.y = this.ShootGroupsProperties["Down-Straight"].BulletSpeed

            setTimeout(function () {
                enemy.destroy()
            }, enemy.DestroyTimer)

            this.BulletsShot++
            this.LastInterval = Date.now()
            if (this.BulletsShot >= this.ShootGroupsProperties["Down-Straight"].NBullets) {
                this.IsShooting = false
                this.BulletsShot = 0
            }
        }
    }

    ShootRadial() {
        let currentAngle = 0
        let angleIncrement = 180 / this.ShootGroupsProperties.Radial.NBullets

        for (let i = 0; i < this.ShootGroupsProperties.Radial.NBullets; i++) {

            let cos = Math.cos(this.DegreesToRadians(currentAngle))
            let sin = Math.sin(this.DegreesToRadians(currentAngle))

            let bullet = this.scene.BossBullets.create()
            bullet.angle += currentAngle
            bullet.body.position.x = this.body.position.x + this.body.halfWidth + (cos * 150)
            bullet.body.position.y = this.body.position.y + this.body.halfHeight + (sin * 150)

            bullet.body.velocity.x = this.ShootGroupsProperties["Radial"].BulletSpeed * cos
            bullet.body.velocity.y = this.ShootGroupsProperties["Radial"].BulletSpeed * sin

            currentAngle += angleIncrement

            setTimeout(function () {
                bullet.destroy()
            }, bullet.DestroyTimer)

        }
        this.IsShooting = false
    }

    DegreesToRadians(degrees) {
        return degrees * (Math.PI / 180)
    }

    Enter() {
        if (this.body.position.y < 50) {
            this.MoveDown()
        }
        else {
            this.body.velocity.y = 0
            this.body.position.y = 50
            this.CurrentFSMState = this.FSM[1]
        }
    }

    MoveDown() {
        this.body.velocity.y = this.Speed
    }

    MoveLeft() {
        this.flipX = true
        this.body.velocity.x = -this.Speed
        if (this.body.position.x <= 0) {
            this.body.position.x = 0
            this.CurrentDirection = this.Directions[1]
        }
    }

    MoveRight() {
        this.flipX = false
        this.body.velocity.x = this.Speed
        if (this.body.position.x >= this.scene.game.config.width - this.body.width) {
            this.body.position.x = this.scene.game.config.width - this.body.width
            this.CurrentDirection = this.Directions[0]
        }
    }
}
export default Boss