import Phaser from "phaser"

class Player extends Phaser.Physics.Arcade.Sprite {

    constructor(scene) {
        let PlayerStartingX = scene.game.config.width / 2
        let PlayerStartingY = scene.game.config.height / 2
        super(scene, PlayerStartingX, PlayerStartingY, "Salvini")

        this.scene = scene
        this.scene.add.existing(this)
        this.scene.physics.world.enable(this)
        this.setCollideWorldBounds(true)
        this.setScale(0.3)

        this.Speed = 10
        this.LatestShoot = Date.now()
        this.ShootCD = 300
        this.LatestShootChange = Date.now()
        this.ShootChangeCD = 300

        this.ShootGroups = ["Up", "Down", "Up-Down", "Radial"]
        this.CurrentShootGroup = 0
        this.BulletTypes = ["BulletNo", "BulletRuspa"]
        this.BulletToShoot = 0

        this.RadialShootBullets = 10

        this.MaxLives = 3
        this.CurrentLives = this.MaxLives
        this.Lives = []

        //temp vector2 to store the starting drawing position of the player lives
        let firstLifeDrawPosition = {
            x: this.scene.game.config.width - 150,
            y: 25
        }

        for (let i = 0; i < this.MaxLives; i++) {
            let life = this.scene.PlayerLives.create()
            life.setScale(0.15, 0.15)
            life.setPosition(firstLifeDrawPosition.x, firstLifeDrawPosition.y)
            firstLifeDrawPosition.x += 60
            this.Lives.push(life)
        }

    }

    GetDamage() {
        this.CurrentLives--
        this.Lives[this.CurrentLives].disableBody(true, true)
        if (this.CurrentLives < 0) {
            this.destroy()
        }

    }

    CheckInput() {
        if (this.scene.MovementKeys.up.isDown) {
            this.MoveUp()
        }
        if (this.scene.MovementKeys.down.isDown) {
            this.MoveDown()
        }
        if (this.scene.MovementKeys.left.isDown) {
            this.MoveLeft()
        }
        if (this.scene.MovementKeys.right.isDown) {
            this.MoveRight()
        }

        if (this.scene.ShootKey.isDown) {
            this.Shoot()
        }

        if (this.scene.ChangeShootKey.isDown && Date.now() - this.LatestShootChange >= this.ShootChangeCD) {
            this.CurrentShootGroup++
            if (this.CurrentShootGroup >= this.ShootGroups.length) {
                this.CurrentShootGroup = 0
            }
            this.LatestShootChange = Date.now()
        }
    }

    MoveUp() {
        this.body.velocity.y -= this.Speed
    }

    MoveDown() {
        this.body.velocity.y += this.Speed
    }

    MoveLeft() {
        this.flipX = true
        this.body.velocity.x -= this.Speed
    }

    MoveRight() {
        this.flipX = false
        this.body.velocity.x += this.Speed
    }

    Shoot() {
        if (Date.now() - this.LatestShoot >= this.ShootCD) {

            switch (this.CurrentShootGroup) {
                case 0:
                    this.BulletToShoot = 0
                    this.ShootUp()
                    break
                case 1:
                    this.BulletToShoot = 1
                    this.ShootDown()
                    break
                case 2:
                    this.ShootUpDown()
                    break
                case 3:
                    this.BulletToShoot = 1
                    this.ShootRadial();
                    break
            }
            this.LatestShoot = Date.now()
        }
    }

    ShootUp() {
        let bullet = this.scene.PlayerBullets.create()
        bullet.setScale(0.5)

        let bulletX = this.body.position.x + this.body.halfWidth
        let bulletY = this.body.position.y

        bullet.setPosition(bulletX, bulletY)
        bullet.body.velocity.y -= bullet.Speed

        setTimeout(function () {
            bullet.destroy()
        }, bullet.DestroyTimer)
    }

    ShootDown() {
        let bullet = this.scene.PlayerBullets.create()
        bullet.setScale(0.5)

        let bulletX = this.body.position.x + this.body.halfWidth
        let bulletY = this.body.position.y + this.body.height

        bullet.setPosition(bulletX, bulletY)
        bullet.body.velocity.y += bullet.Speed

        setTimeout(function () {
            bullet.destroy()
        }, bullet.DestroyTimer)
    }

    ShootUpDown() {
        this.BulletToShoot = 0
        this.ShootUp()
        this.BulletToShoot = 1
        this.ShootDown()
    }

    ShootRadial() {
        let currentAngle = 0
        let angleIncrement = 360 / this.RadialShootBullets

        for (let i = 0; i < this.RadialShootBullets; i++) {

            let bullet = this.scene.PlayerBullets.create()
            bullet.setScale(0.5)

            bullet.angle += currentAngle

            let cos = Math.cos(this.DegreesToRadians(currentAngle))
            let sin = Math.sin(this.DegreesToRadians(currentAngle))

            //starting radial position exactly at the center of the player ship
            bullet.body.position.x = this.body.position.x + this.body.halfWidth + (cos * 50)
            bullet.body.position.y = this.body.position.y + this.body.halfHeight + (sin * 50)

            bullet.body.velocity.x = bullet.Speed * cos
            bullet.body.velocity.y = bullet.Speed * sin

            currentAngle += angleIncrement

            setTimeout(function () {
                bullet.destroy()
            }, bullet.DestroyTimer)
        }
    }

    DegreesToRadians(degrees) {
        return degrees * (Math.PI / 180)
    }

}

export default Player