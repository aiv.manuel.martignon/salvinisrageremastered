class Persona {

    constructor(nome = "N.D.", cognome = "N.D.") {
        this.Nome = nome
        this.Cognome = cognome
    }

    SayHello() {
        console.log("Ciao, mi chiamo " + this.Nome + " " + this.Cognome)
    }
}

class Studente extends Persona {
    constructor(nome, cognome, matricola) {
        super(nome, cognome)
        this.Matricola = matricola
    }

    SayHello() {
        console.log(`ciao sono ${this.Nome} ${this.Cognome} matricola ${this.Matricola}`)
    }
}

export default Persona //export primario, riguarda solo ciò che serve per far funzionare la classe
export { Studente }
//export { f, d } //export secondario, usato per prendere altri elementi