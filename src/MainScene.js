import Phaser from "phaser"
import Player from "./Player"
import Enemy from "./Enemy"
import PlayerBullet from "./PlayerBullet"
import Boss from "./Boss"
import BossBullet from "./BossBullet"

import SalviniImg from "./assets/salvini.png"
import Immigrant0Img from "./assets/immigrant0.png"
import Immigrant1Img from "./assets/immigrant1.png"
import NoBulletImg from "./assets/salvini-bullet.png"
import RuspaBulletImg from "./assets/salvini-bullet-1.png"
import LifeBarImg from "./assets/health.png"
import HeartImg from "./assets/heart.png"
import BossImg from "./assets/ship.png"

class MainScene extends Phaser.Scene {

    preload() {
        this.load.image("Salvini", SalviniImg)
        this.load.image("Immigrant0", Immigrant0Img)
        this.load.image("Immigrant1", Immigrant1Img)
        this.load.image("BulletNo", NoBulletImg)
        this.load.image("BulletRuspa", RuspaBulletImg)
        this.load.image("LifeBar", LifeBarImg)
        this.load.image("PlayerLife", HeartImg)
        this.load.image("Boss", BossImg)
    }

    create() {

        this.PlayerLives = this.physics.add.group({
            defaultKey: "PlayerLife"
        })

        //there is only one player, so i can construct it using new keyword.
        //doing like this, i need to pass the scene to the ctor manually
        this.Player = new Player(this)

        //since i am using a factory to handle creation of player bullets, i will use phaser scene's create method.
        //phaser will automatically handle the constructor getting the current scene by itself
        this.PlayerBullets = this.physics.add.group({
            classType: PlayerBullet
        })

        this.EnemySpawnCD = 1000
 
        this.LifeBars = this.physics.add.group({
            defaultKey: "LifeBar"
        })
        
        this.Enemies = this.physics.add.group({
            classType: Enemy
        })

        this.BossBullets = this.physics.add.group({
            classType: BossBullet
        })

        let sceneT = this

        //phaser's way to implement a timed function
        this.SpawnEnemyEvent = this.time.addEvent({
            delay: sceneT.EnemySpawnCD,
            loop: true,
            callback: function () {
                let enemy = sceneT.Enemies.create()
                enemy.Attack()
            }
        })

        this.BossSpawned = false
        this.BossSpawnScore = 100
        //define physics groups and the relative collision callbacks
        this.physics.add.overlap(this.PlayerBullets, this.Enemies, this.PlayerBulletsEnemiesOverlap)
        this.physics.add.overlap(this.PlayerBullets, this.BossBullets, this.PlayerBulletsEnemiesOverlap)
        this.physics.add.overlap(this.Player, this.Enemies, this.PlayerEnemiesOverlap)
        this.physics.add.overlap(this.Player, this.BossBullets, this.PlayerEnemiesOverlap)
        //define the input keys
        this.MovementKeys = this.input.keyboard.createCursorKeys()
        this.ShootKey = this.input.keyboard.addKey("SPACE")
        this.ChangeShootKey = this.input.keyboard.addKey("CTRL")
        
        this.PlayerScore = 0
        this.ScoreText = this.add.text(16, 16, 'Sbarchi Respinti: ' + this.PlayerScore, { fontFamily: "'Luckiest Guy'", fontSize: '32px', fill: '#000' });
    }
    
    update() {
        this.Player.CheckInput()
        
        if(this.BossSpawned)
        {
            this.Boss.UpdateBehaviour()
            this.UpdateLifeBarsPositions(this.BossBullets)
        }
        
        this.UpdateLifeBarsPositions(this.Enemies)
    }
    
    SpawnBoss()
    {
        this.Boss = new Boss(this)
        this.physics.add.overlap(this.PlayerBullets, this.Boss, this.PlayerBulletsBossOverlap)
        this.BossSpawned = true
    }

    UpdateLifeBarsPositions(array) {
        let activeEnemies = array.getChildren()
        if (activeEnemies.length > 0) {
            for (let i = 0; i < activeEnemies.length; i++) {
                let lifeX = activeEnemies[i].body.position.x + activeEnemies[i].body.halfWidth
                let lifeY = activeEnemies[i].body.position.y + activeEnemies[i].body.height + 5
                activeEnemies[i].LifeBar.setPosition(lifeX, lifeY)
            }
        }
    }

    PlayerBulletsEnemiesOverlap(bullet, enemy) {
        enemy.GetDamage(bullet.Damage)
        bullet.destroy()
    }

    PlayerEnemiesOverlap(player, enemy)
    {
        player.GetDamage()
        enemy.GetDamage(enemy.MaxLife)
    }

    PlayerBulletsBossOverlap(boss, bullet)
    {
        boss.GetDamage(bullet.Damage)
        bullet.destroy()
    }
}

export default MainScene