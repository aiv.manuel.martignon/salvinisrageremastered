import Phaser from "phaser";
import MainScene from "./MainScene"

const config = {
    type: Phaser.AUTO,
    backgroundColor: '#0FF',
    parent: "phaser-example",
    width: 1900,
    height: 900,
    physics: {
        default: "arcade",
        arcade: {
            gravity: { x: 0, y: 0 },
            debug: false
        }
    },
    scene: [MainScene]
};

const game = new Phaser.Game(config);